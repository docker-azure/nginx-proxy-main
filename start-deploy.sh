. ./env.sh

docker-compose -f docker-compose.yml -p $APP_NAME_DOCKER down

docker network rm ${DOCKER_NETWORK}
docker network create $DOCKER_NETWORK

docker-compose -f docker-compose.yml -p $APP_NAME_DOCKER up --build -d
